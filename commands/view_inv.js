async function gsheet(message, args, deps) {
    if ((args[0] == "help" && args.length == 1)) {
         message.channel.send("Usage: \`/view_inv\`");
         return;
    }

    const doc_id = await deps.keyv.get(message.author.id);

    var doc = new deps.GoogleSpreadsheet(doc_id);
    await doc.useServiceAccountAuth(deps.creds);

    await doc.loadInfo();

    const sheet = doc.sheetsByIndex[0];
    await sheet.loadCells();

    var desc = "";
    var i = 1;
    var name = sheet.getCell(i, 4);
    var quan = sheet.getCell(i, 3);
    var weig = sheet.getCell(i, 5);
    while (name.value != null) {
        desc = desc + "\n" + name.value + " (" + quan.value + "), " + weig.value + "Kg";
        i++;
        name = sheet.getCell(i, 4);
        quan = sheet.getCell(i, 3);
        weig = sheet.getCell(i, 5);
    }

    const weight = sheet.getCellByA1("I2");
    const capacity = sheet.getCellByA1("J2");

    const full = 100.0*weight.value/capacity.value;

    const embed = new deps.Discord.MessageEmbed()
        .setTitle("Your inventory!")
        .addFields(
            {name: "Transported weight", value: weight.value + " Kg"},
            {name: "Capacity", value: capacity.value + " Kg"},
        )
        .setDescription(desc);

    message.reply(embed);
}

module.exports = {
    name: 'view_inv',
    description: 'View inventory',
    execute(message, args, deps) {
	    gsheet(message, args, deps);
    },
};

