async function gsheet(message, args, deps) {
    if ((args[0] == "help" && args.length == 1)) {
         message.reply("Usage: \`/remove_from_inv item_name number (optional, default 1, valid values: int or all)\`");
         return;
    }

    args.reverse();
    var quant = 1;
    var last = args.shift();
    var name = "";

    if (isNaN(last) && last != "all") {
        quant = 1;
        name = last;
    } else if (last == "all") {
        quant = last;
    } else {
        quant = parseInt(last);
    }

    while ((i = args.shift()) != undefined) {
       name = i + " " + name;
    }

    const doc_id = await deps.keyv.get(message.author.id)

    var doc = new deps.GoogleSpreadsheet(doc_id);
    await doc.useServiceAccountAuth(deps.creds);

    await doc.loadInfo();

    const sheet = doc.sheetsByIndex[0];
    await sheet.loadCells();

    var i = 1;
    var cell = sheet.getCell(i, 4);
    while (cell.value != null && cell.value.trim() != name.trim()) {
        i++;
        cell = sheet.getCell(i, 4);
    }

    var l = i+1;
    var last = sheet.getCell(l, 4);
    while (last.value != null) {
        l++;
        last = sheet.getCell(l, 4);
    }
    l = l-1;
    last = sheet.getCell(l, 4);

    if (cell.value == null) {
        message.reply("Item not found!");
        return;
    }

    q = sheet.getCell(i, 3);
    lq = sheet.getCell(l, 3);
    if (quant == "all") {
        quant = 0;
    } else {
        quant = parseInt(q.value) - quant;
    }

    if (quant > 0) {
        q.value = quant;
    } else {
        if (last.value == null) {
            cell.value = null;
            cell = sheet.getCell(i, 5);
            cell.value = null;
            q.value = null;
        } else {
            cell.value = last.value;
            last.value = null;
            cell = sheet.getCell(i, 5);
            last = sheet.getCell(l, 5);
            cell.value = last.value;
            last.value = null;
            q.value = lq.value;
            lq.value = null;
        }
    }
    await sheet.saveUpdatedCells();
    await sheet.loadCells("I2:J2");

    const weight = sheet.getCellByA1("I2");
    const capacity = sheet.getCellByA1("J2");

    const full = 100.0*weight.value/capacity.value;

    const embed = new deps.Discord.MessageEmbed()
        .setTitle("Successfully removed " + name + " from inventory!")
        .addFields(
            {name: "Transported weight", value: weight.value + " Kg"},
            {name: "Capacity", value: capacity.value + " Kg"},
        )
        .setDescription("Full at " + full.toString() + "%");

    message.reply(embed);
}

module.exports = {
    name: 'remove_from_inv',
    description: 'Remove something from your inventory',
    execute(message, args, deps) {
	    gsheet(message, args, deps);
    },
};

