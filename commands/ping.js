module.exports = {
	name: 'ping',
	description: 'Ping!',
	execute(message, args, deps) {
		message.channel.send('Pong.');
	},
};
