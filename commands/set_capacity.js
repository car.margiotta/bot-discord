async function gsheet(message, args, deps) {
    if ((args[0] == "help" && args.length == 1) || args.length != 1 ) {
         message.channel.send("Usage: \`/set_capacity capacity\`");
         return;
    }

    var cap = 0;

    try {
        cap = parseInt(args[0]);
    } catch(e) {
        message.channel.send("Usage: \`/set_capacity capacity\`");
        return;
    }
    const doc_id = await deps.keyv.get(message.author.id);

    var doc = new deps.GoogleSpreadsheet(doc_id);
    await doc.useServiceAccountAuth(deps.creds);

    await doc.loadInfo();

    const sheet = doc.sheetsByIndex[0];
    await sheet.loadCells();

    const cell = sheet.getCellByA1("J2");

    cell.value = cap;

    await sheet.saveUpdatedCells();

    const embed = new deps.Discord.MessageEmbed()
       .setTitle("Capacity updated!")
       .addFields(
           {name: "New capacity", value: cell.value},
       );

    message.reply(embed);
}

module.exports = {
    name: 'set_capacity',
    description: 'Change backpack capacity',
    execute(message, args, deps) {
	    gsheet(message, args, deps);
    },
};

