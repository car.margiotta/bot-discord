async function gsheet(message, args, deps) {
    if ((args[0] == "help" && args.length == 1)) {
         message.reply("Usage: \`/add_to_inv item_name item_weight number (optional, default 1)\`");
         return;
    }

    args.reverse();
    var quant = 1;
    var w = 0.0;
    var last = args.shift();
    var last1 = args.shift();
    var name = "";

    if (isNaN(last)) {
        message.reply("Usage: \`/add_to_inv item_name item_weight number (optional, default 1)\`");
        return;
    }

    if (isNaN(last1)) {
        quant = 1;
        w = parseFloat(last);
        name = last1;
    } else {
        quant = parseInt(last);
        w = parseFloat(last1);
    }

    while ((i = args.shift()) != undefined) {
       name = i + " " + name;
    }

    const doc_id = await deps.keyv.get(message.author.id)

    var doc = new deps.GoogleSpreadsheet(doc_id);
    await doc.useServiceAccountAuth(deps.creds);

    await doc.loadInfo();

    const sheet = doc.sheetsByIndex[0];
    await sheet.loadCells();

    var i = 1;
    var cell = sheet.getCell(i, 4);
    while (cell.value != null && cell.value != name) {
        i++;
        cell = sheet.getCell(i, 4);
    }

    cell.value = name;
    cell = sheet.getCell(i, 3);

    cell.value = (cell.value == null) ? quant : (parseInt(cell.value) + quant);
    cell = sheet.getCell(i, 5);
    cell.value = w;
    await sheet.saveUpdatedCells();
    await sheet.loadCells("I2:J2");

    const weight = sheet.getCellByA1("I2");
    const capacity = sheet.getCellByA1("J2");

    const full = 100.0*weight.value/capacity.value;

    const embed = new deps.Discord.MessageEmbed()
        .setTitle("Successfully added " + name + " to inventory!")
        .addFields(
            {name: "Transported weight", value: weight.value + " Kg"},
            {name: "Capacity", value: capacity.value + " Kg"},
        )
        .setDescription("Full at " + full.toString() + "%");

    message.reply(embed);
}

module.exports = {
    name: 'add_to_inv',
    description: 'Add something to your inventory',
    execute(message, args, deps) {
	    gsheet(message, args, deps);
    },
};

