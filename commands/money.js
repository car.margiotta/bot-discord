async function gsheet(message, args, deps) {
    if ((args[0] == "help" && args.length == 1) || (args[0] != "gold" && args[0] != "copper" && args[0] != "silver" && args[0] != "platinum" && args[0] != "electrum" ) || (args[1] != "add" && args[1] != "pay" && args[1] != "view") || (args[2] != null && args[1] == "view") || (args[2] == null && args[1] != "view") ) {
         message.channel.send("Usage: \`/money type operation(add, pay or view) quantity (only for add or pay)\`");
         return;
    }

    const operation = args[1];
    const money = args[0];
    var quant = 0;

    try {
        quant = parseInt(args[2]);
    } catch(e) {
        message.channel.send("Usage: \`/money type operation(add, pay or view) quantity (only for add or pay)\`");
        return;
    }
    const doc_id = await deps.keyv.get(message.author.id);

    var doc = new deps.GoogleSpreadsheet(doc_id);
    await doc.useServiceAccountAuth(deps.creds);

    await doc.loadInfo();

    const sheet = doc.sheetsByIndex[0];
    await sheet.loadCells();

    const money_cells = {
        "copper": "B2",
        "silver": "B3",
        "electrum": "B4",
        "gold": "B5",
        "platinum": "B6",
    };

    const cell = sheet.getCellByA1(money_cells[money]);

    if (operation == "add") {
        cell.value = parseInt(cell.value) + quant;
    } else if (operation == "pay") {
        cell.value = parseInt(cell.value) - quant;
    }

    await sheet.saveUpdatedCells();

    var embed = new deps.Discord.MessageEmbed()
       .setTitle("Money, get away!")
       .addFields(
           {name: money + " coins", value: cell.value},
       );

    if (operation != "view") {
        embed.setTitle("You " + (operation == "pay" ? "paid" : "added") + " money!");
    }

    message.reply(embed);
}

module.exports = {
    name: 'money',
    description: 'Manage money',
    execute(message, args, deps) {
	    gsheet(message, args, deps);
    },
};

