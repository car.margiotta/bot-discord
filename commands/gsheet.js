async function gsheet(message, args, deps) {
    if (args.length != 1 || !args[0].startsWith("https://") || args[0].startsWith("help")) {
         message.channel.send("Copy this sheet https://docs.google.com/spreadsheets/d/1wtgMgNXhg_GQE4HcqYGPAnhQPLULBAtJd7YjacKnf24/edit?usp=sharing and share it with discordinventorybot@discordinventorybot.iam.gserviceaccount.com");
         return;
    }
    var doc = new deps.GoogleSpreadsheet(args[0].split('/')[5]);
    await doc.useServiceAccountAuth(deps.creds);

    await doc.loadInfo();

    const sheet = doc.sheetsByIndex[0];
    await sheet.loadCells('A1:B6');
    await sheet.loadCells('I2:J2');

    const cp = sheet.getCellByA1('B2');
    const sp = sheet.getCellByA1('B3');
    const ep = sheet.getCellByA1('B4');
    const gp = sheet.getCellByA1('B5');
    const pp = sheet.getCellByA1('B6');

    const weight = sheet.getCellByA1('I2');
    const capacity = sheet.getCellByA1('J2');

    const embed = new deps.Discord.MessageEmbed()
        .setTitle("Correctly loaded!")
        .setURL(args[0])
        .addFields(
            {name: "Copper", value: cp.value, inline: true},
            {name: "Silver", value: sp.value, inline: true},
            {name: "Electrum", value: ep.value, inline: true},
            {name: "Gold", value: gp.value, inline: true},
            {name: "Platinum", value: pp.value, inline: true},
            {name: '\u200B', value: '\u200B'},
            {name: "Transported weight", value: weight.value + " Kg"},
            {name: "Capacity", value: capacity.value + " Kg"},
        );

    await deps.keyv.set(message.author.id, args[0].split('/')[5]);
    message.channel.send(embed);
}

module.exports = {
    name: 'gsheet',
    description: 'Import sheet from Google Sheet',
    execute(message, args, deps) {
	    gsheet(message, args, deps);
    },
};

