const {GoogleSpreadsheet} = require('google-spreadsheet');
const fs = require('fs');
const Discord = require('discord.js');
const creds = require('./discordinventorybot-f7d4d8459b54.json');
const Keyv = require('keyv');

const {prefix, token} = require('./config.json');

const keyv = new Keyv('sqlite://discord_sheets.sqlite');
keyv.on('error', err => console.error('Keyv connection error:', err));

const client = new Discord.Client();
client.commands = new Discord.Collection();

const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

client.once('ready', () => {
	console.log('Ready!');
});

for (const file of commandFiles) {
	const command = require(`./commands/${file}`);

	// set a new item in the Collection
	// with the key as the command name and the value as the exported module
	client.commands.set(command.name, command);
}


client.login(token);

client.on('message', message => {
	if (!message.content.startsWith(prefix) || message.author.bot) return;

	const args = message.content.slice(prefix.length).split(' ');
	const command = args.shift().toLowerCase();

	try {
		client.commands.get(command).execute(message, args, {"GoogleSpreadsheet": GoogleSpreadsheet, "creds": creds, "Discord": Discord, "keyv": keyv});
	} catch (error) {
		console.error(error);
		message.reply('there was an error trying to execute that command!');
	}
});
